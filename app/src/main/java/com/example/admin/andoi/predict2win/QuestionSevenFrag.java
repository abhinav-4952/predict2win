package com.example.admin.andoi.predict2win;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;


/**
 * A simple {@link Fragment} subclass.
 */
public class QuestionSevenFrag extends Fragment {

    private TextView ques_seven_display;
    private Button seven_opta,seven_optb,seven_optc,seven_optd;
    private DatabaseReference ques_val,opta,optb,optc,optd,sevenques;
    public QuestionSevenFrag() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View root=inflater.inflate(R.layout.fragment_question_seven, container, false);

        ques_seven_display=(TextView)root.findViewById(R.id.question_seven_disp);
        seven_opta=(Button)root.findViewById(R.id.ques_seven_opta);
        seven_optb=(Button)root.findViewById(R.id.ques_seven_optb);
        seven_optc=(Button)root.findViewById(R.id.ques_seven_optc);
        seven_optd=(Button)root.findViewById(R.id.ques_seven_optd);
        sevenques=FirebaseDatabase.getInstance().getReference().child("Question_seven");
        sevenques.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                ques_seven_display.setText(dataSnapshot.child("Question_seven_dis").getValue().toString());
                seven_opta.setText(dataSnapshot.child("seven_opta_dis").getValue().toString());
                seven_optb.setText(dataSnapshot.child("seven_optb_dis").getValue().toString());
                seven_optc.setText(dataSnapshot.child("seven_optc_dis").getValue().toString());
                seven_optd.setText(dataSnapshot.child("seven_optd_dis").getValue().toString());
                //String btn_status=dataSnapshot.child("FirstButton_enable").getValue().toString().toLowerCase();


            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        seven_opta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                MainActivity.res_val=MainActivity.res_val+"7-a.";
                FragmentTransaction fg=getFragmentManager().beginTransaction();
                fg.replace(R.id.fagview,new QuestionEightFrag());
                fg.commit();
            }
        });

        seven_optb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                MainActivity.res_val=MainActivity.res_val+"7-b.";
                FragmentTransaction fg=getFragmentManager().beginTransaction();
                fg.replace(R.id.fagview,new QuestionEightFrag());
                fg.commit();
            }
        });

        seven_optc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                MainActivity.res_val=MainActivity.res_val+"7-c.";
                FragmentTransaction fg=getFragmentManager().beginTransaction();
                fg.replace(R.id.fagview,new QuestionEightFrag());
                fg.commit();
            }
        });

        seven_optd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                MainActivity.res_val=MainActivity.res_val+"7-d.";

                FragmentTransaction fg=getFragmentManager().beginTransaction();
                fg.replace(R.id.fagview,new QuestionEightFrag());
                fg.commit();
            }
        });



        return root;
    }

}
