package com.example.admin.andoi.predict2win;

import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;

/**
 * Created by user on 5/21/2018.
 */

public class MyFirebaseInstanceIdService extends com.google.firebase.iid.FirebaseInstanceIdService {
    private static final String TAG = "MyFirebaseIIDService";

    @Override
    public void onTokenRefresh() {
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();

        //Displaying token in logcat
        Log.e(TAG, "Refreshed token: " + refreshedToken);
        super.onTokenRefresh();
    }
    private void sendRegistrationToServer(String token) {

    }
}
