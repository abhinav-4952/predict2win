package com.example.admin.andoi.predict2win;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;


/**
 * A simple {@link Fragment} subclass.
 */
public class QuestionNineFrag extends Fragment {
    private TextView ques_nine_display;
    private Button nine_opta,nine_optb,nine_optc,nine_optd;
    private DatabaseReference ques_val,opta,optb,optc,optd,nineques;

    public QuestionNineFrag() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View root=inflater.inflate(R.layout.fragment_question_nine, container, false);

        ques_nine_display=(TextView)root.findViewById(R.id.question_nine_disp);
        nine_opta=(Button)root.findViewById(R.id.ques_nine_opta);
        nine_optb=(Button)root.findViewById(R.id.ques_nine_optb);
        nine_optc=(Button)root.findViewById(R.id.ques_nine_optc);
        nine_optd=(Button)root.findViewById(R.id.ques_nine_optd);
        nineques=FirebaseDatabase.getInstance().getReference().child("Question_nine");
        nineques.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                ques_nine_display.setText(dataSnapshot.child("Question_nine_dis").getValue().toString());
                nine_opta.setText(dataSnapshot.child("nine_opta_dis").getValue().toString());
                nine_optb.setText(dataSnapshot.child("nine_optb_dis").getValue().toString());
                nine_optc.setText(dataSnapshot.child("nine_optc_dis").getValue().toString());
                nine_optd.setText(dataSnapshot.child("nine_optd_dis").getValue().toString());
                //String btn_status=dataSnapshot.child("FirstButton_enable").getValue().toString().toLowerCase();


            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        nine_opta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                MainActivity.res_val=MainActivity.res_val+"9-a.";
                FragmentTransaction fg=getFragmentManager().beginTransaction();
                fg.replace(R.id.fagview,new QuestionTenFrag());
                fg.commit();
            }
        });

        nine_optb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                MainActivity.res_val=MainActivity.res_val+"9-b.";
                FragmentTransaction fg=getFragmentManager().beginTransaction();
                fg.replace(R.id.fagview,new QuestionTenFrag());
                fg.commit();
            }
        });

        nine_optc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                MainActivity.res_val=MainActivity.res_val+"9-c.";
                FragmentTransaction fg=getFragmentManager().beginTransaction();
                fg.replace(R.id.fagview,new QuestionTenFrag());
                fg.commit();
            }
        });

        nine_optd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                MainActivity.res_val=MainActivity.res_val+"9-d.";

                FragmentTransaction fg=getFragmentManager().beginTransaction();
                fg.replace(R.id.fagview,new QuestionTenFrag());
                fg.commit();
            }
        });



        return root;
    }

}
