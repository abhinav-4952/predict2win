package com.example.admin.andoi.predict2win;


import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 */
public class PassCodeFrag extends Fragment {

    private DatabaseReference mDb,lbview;
    public int pass_Code,entered_passCode;
    public String retrivedValue,enteredValue;
    public Button ValidateButton,view_board;
    public ImageButton logout;
    public EditText Value;
    public TextView txt;
    public static String checked_passcode="";
    public static String passcodecontain="";
    public static ArrayList<String> enteval=new ArrayList<>();
    public PassCodeFrag() {
        // Required empty public constructor
    }
    public String result="";
    public String lb_status="";


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View root=inflater.inflate(R.layout.fragment_pass_code, container, false);
        ValidateButton=(Button)root.findViewById(R.id.submitPassCode);
        logout=(ImageButton)root.findViewById(R.id.logot);
        view_board=(Button)root.findViewById(R.id.Leaderboard);
        //txt=(TextView)root.findViewById(R.id.entervalue);
        //txt.setText(passcodecontain);
        Value=(EditText)root.findViewById(R.id.Code);
        // txt=(TextView) root.findViewById(R.id.textView);
        mDb= FirebaseDatabase.getInstance().getReference().child("passcode");
        lbview=FirebaseDatabase.getInstance().getReference().child("Leader_Board");

        mDb.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                retrivedValue=dataSnapshot.getValue().toString();
                //pass_Code=Integer.parseInt(retrivedValue);

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
        lbview.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                lb_status=dataSnapshot.child("Leaderboard_status").getValue().toString().toLowerCase();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
        view_board.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(lb_status.equals("enable")){

                    Intent intent = new Intent(getActivity(), Leaderboard_page.class);
                    startActivity(intent);
                }else{
                    Toast.makeText(getActivity(), "Can View Only After the match",
                            Toast.LENGTH_LONG).show();
                }
            }
        });
        //entered_passCode=Integer.parseInt(enteredValue);
        ValidateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                enteredValue=Value.getText().toString();

                // txt.setText(retrivedValue);

                if(haveNetworkConnection()){
                    if((!(checked_passcode.equals(enteredValue))) && (!(passcodecontain.contains(enteredValue)))){
                        if(enteredValue.isEmpty()){
                            Toast.makeText(getActivity(), "Enter PassCode",
                                    Toast.LENGTH_LONG).show();
                        }else if((enteredValue.equals(retrivedValue)) ){

                            checked_passcode=enteredValue;
                            passcodecontain=passcodecontain+enteredValue;
                            FragmentTransaction fg=getFragmentManager().beginTransaction();
                            fg.replace(R.id.fagview,new QuestionOneFrag());
                            fg.commit();
                        }else{
                            Toast.makeText(getActivity(), "Invalid",
                                    Toast.LENGTH_LONG).show();
                        }
                    }else {
                        Toast.makeText(getActivity(), "Already entered in League",
                                Toast.LENGTH_LONG).show();
                    }
                }else{
                    Toast.makeText(getActivity(), "Check your Internet Connectivity",
                            Toast.LENGTH_LONG).show();
                }
            }
        });
        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FirebaseAuth.getInstance().signOut();
                Intent in = new Intent(getActivity(), MainActivity.class);
                startActivity(in);
            }
        });

        return root;
    }
    private boolean haveNetworkConnection() {
        boolean haveConnectedWifi = false;
        boolean haveConnectedMobile = false;

        ConnectivityManager cm = (ConnectivityManager)getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo[] netInfo = cm.getAllNetworkInfo();
        for (NetworkInfo ni : netInfo) {
            if (ni.getTypeName().equalsIgnoreCase("WIFI"))
                if (ni.isConnected())
                    haveConnectedWifi = true;
            if (ni.getTypeName().equalsIgnoreCase("MOBILE"))
                if (ni.isConnected())
                    haveConnectedMobile = true;
        }
        return haveConnectedWifi || haveConnectedMobile;
    }

}
