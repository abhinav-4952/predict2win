package com.example.admin.andoi.predict2win;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;


/**
 * A simple {@link Fragment} subclass.
 */
public class QuestionEightFrag extends Fragment {
    private TextView ques_eight_display;
    private Button eight_opta,eight_optb,eight_optc,eight_optd;
    private DatabaseReference ques_val,opta,optb,optc,optd,eightques;

    public QuestionEightFrag() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View root=inflater.inflate(R.layout.fragment_question_eight, container, false);

        ques_eight_display=(TextView)root.findViewById(R.id.question_eight_disp);
        eight_opta=(Button)root.findViewById(R.id.ques_eight_opta);
        eight_optb=(Button)root.findViewById(R.id.ques_eight_optb);
        eight_optc=(Button)root.findViewById(R.id.ques_eight_optc);
        eight_optd=(Button)root.findViewById(R.id.ques_eight_optd);
        eightques=FirebaseDatabase.getInstance().getReference().child("Question_eight");
        eightques.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                ques_eight_display.setText(dataSnapshot.child("Question_eight_dis").getValue().toString());
                eight_opta.setText(dataSnapshot.child("eight_opta_dis").getValue().toString());
                eight_optb.setText(dataSnapshot.child("eight_optb_dis").getValue().toString());
                eight_optc.setText(dataSnapshot.child("eight_optc_dis").getValue().toString());
                eight_optd.setText(dataSnapshot.child("eight_optd_dis").getValue().toString());
                //String btn_status=dataSnapshot.child("FirstButton_enable").getValue().toString().toLowerCase();


            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
        eight_opta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                MainActivity.res_val=MainActivity.res_val+"8-a.";
                FragmentTransaction fg=getFragmentManager().beginTransaction();
                fg.replace(R.id.fagview,new QuestionNineFrag());
                fg.commit();
            }
        });

        eight_optb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                MainActivity.res_val=MainActivity.res_val+"8-b.";
                FragmentTransaction fg=getFragmentManager().beginTransaction();
                fg.replace(R.id.fagview,new QuestionNineFrag());
                fg.commit();
            }
        });

        eight_optc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                MainActivity.res_val=MainActivity.res_val+"8-c.";
                FragmentTransaction fg=getFragmentManager().beginTransaction();
                fg.replace(R.id.fagview,new QuestionNineFrag());
                fg.commit();
            }
        });

        eight_optd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                MainActivity.res_val=MainActivity.res_val+"8-d.";

                FragmentTransaction fg=getFragmentManager().beginTransaction();
                fg.replace(R.id.fagview,new QuestionNineFrag());
                fg.commit();
            }
        });


        return root;
    }

}
