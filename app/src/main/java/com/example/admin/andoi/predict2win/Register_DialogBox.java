package com.example.admin.andoi.predict2win;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatDialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

/**
 * Created by user on 5/22/2018.
 */

public class Register_DialogBox extends AppCompatDialogFragment {
    private EditText email,passwrod;
    private ExampleListener el;
    @Override

    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder=new AlertDialog.Builder(getActivity());
        LayoutInflater inflater=getActivity().getLayoutInflater();
        View view=inflater.inflate(R.layout.layout_dialog,null);
        builder.setView(view).setTitle("Register").setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

            }
        }).setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                    String nme=email.getText().toString().trim();
                    String word=passwrod.getText().toString().trim();
                    el.applyText(nme,word);


            }
        });
        email=(EditText)view.findViewById(R.id.register_username);
        passwrod=(EditText)view.findViewById(R.id.register_password);

        return builder.create();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try{el=(ExampleListener) context;
        }catch(ClassCastException e){
            throw new ClassCastException(context.toString()+"Must");
        }
    }

    public interface ExampleListener{
        void applyText(String user,String passw);
    }
}
