package com.example.admin.andoi.predict2win;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;


/**
 * A simple {@link Fragment} subclass.
 */
public class QuestionTwoFrag extends Fragment {

    private TextView ques_two_display;
    private Button two_opta,two_optb,two_optc,two_optd;
    private DatabaseReference ques_val,opta,optb,optc,optd,twoques;
    public QuestionTwoFrag() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View root =inflater.inflate(R.layout.fragment_question_two, container, false);
        ques_two_display=(TextView)root.findViewById(R.id.question_two_disp);
        two_opta=(Button)root.findViewById(R.id.ques_two_opta);
        two_optb=(Button)root.findViewById(R.id.ques_two_optb);
        two_optc=(Button)root.findViewById(R.id.ques_two_optc);
        two_optd=(Button)root.findViewById(R.id.ques_two_optd);
        twoques=FirebaseDatabase.getInstance().getReference().child("Question_two");
        twoques.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                ques_two_display.setText(dataSnapshot.child("Question_two_dis").getValue().toString());
                two_opta.setText(dataSnapshot.child("two_opta_dis").getValue().toString());
                two_optb.setText(dataSnapshot.child("two_optb_dis").getValue().toString());
                two_optc.setText(dataSnapshot.child("two_optc_dis").getValue().toString());
                two_optd.setText(dataSnapshot.child("two_optd_dis").getValue().toString());
                //String btn_status=dataSnapshot.child("FirstButton_enable").getValue().toString().toLowerCase();
                

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
        two_opta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                MainActivity.res_val=MainActivity.res_val+"2-a.";
                FragmentTransaction fg=getFragmentManager().beginTransaction();
                fg.replace(R.id.fagview,new QuestionThreeFrag());
                fg.commit();
            }
        });

        two_optb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                MainActivity.res_val=MainActivity.res_val+"2-b.";
                FragmentTransaction fg=getFragmentManager().beginTransaction();
                fg.replace(R.id.fagview,new QuestionThreeFrag());
                fg.commit();
            }
        });

        two_optc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                MainActivity.res_val=MainActivity.res_val+"2-c.";
                FragmentTransaction fg=getFragmentManager().beginTransaction();
                fg.replace(R.id.fagview,new QuestionThreeFrag());
                fg.commit();
            }
        });

        two_optd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                MainActivity.res_val=MainActivity.res_val+"2-d.";
                FragmentTransaction fg=getFragmentManager().beginTransaction();
                fg.replace(R.id.fagview,new QuestionThreeFrag());
                fg.commit();
            }
        });

        return root;
    }

}
