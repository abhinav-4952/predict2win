package com.example.admin.andoi.predict2win;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;


/**
 * A simple {@link Fragment} subclass.
 */
public class QuestionThreeFrag extends Fragment {

    private TextView ques_three_display;
    private Button three_opta,three_optb,three_optc,three_optd;
    private DatabaseReference ques_val,opta,optb,optc,optd,threeques;
    public QuestionThreeFrag() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        
        View root= inflater.inflate(R.layout.fragment_question_three, container, false);
        ques_three_display=(TextView)root.findViewById(R.id.question_three_disp);
        three_opta=(Button)root.findViewById(R.id.ques_three_opta);
        three_optb=(Button)root.findViewById(R.id.ques_three_optb);
        three_optc=(Button)root.findViewById(R.id.ques_three_optc);
        three_optd=(Button)root.findViewById(R.id.ques_three_optd);
        threeques=FirebaseDatabase.getInstance().getReference().child("Question_three");
        threeques.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                ques_three_display.setText(dataSnapshot.child("Question_three_dis").getValue().toString());
                three_opta.setText(dataSnapshot.child("three_opta_dis").getValue().toString());
                three_optb.setText(dataSnapshot.child("three_optb_dis").getValue().toString());
                three_optc.setText(dataSnapshot.child("three_optc_dis").getValue().toString());
                three_optd.setText(dataSnapshot.child("three_optd_dis").getValue().toString());
                //String btn_status=dataSnapshot.child("FirstButton_enable").getValue().toString().toLowerCase();


            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
        three_opta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(getActivity(), "Option a",
                        Toast.LENGTH_LONG).show();
                MainActivity.res_val=MainActivity.res_val+"3-a.";
                FragmentTransaction fg=getFragmentManager().beginTransaction();
                fg.replace(R.id.fagview,new QuestionFourFrag());
                fg.commit();
            }
        });

        three_optb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(getActivity(), "Option b",
                        Toast.LENGTH_LONG).show();
                MainActivity.res_val=MainActivity.res_val+"3-b.";
                FragmentTransaction fg=getFragmentManager().beginTransaction();
                fg.replace(R.id.fagview,new QuestionFourFrag());
                fg.commit();
            }
        });

        three_optc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(getActivity(), "Option C",
                        Toast.LENGTH_LONG).show();
                MainActivity.res_val=MainActivity.res_val+"3-c.";
                FragmentTransaction fg=getFragmentManager().beginTransaction();
                fg.replace(R.id.fagview,new QuestionFourFrag());
                fg.commit();
            }
        });

        three_optd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(getActivity(), "Option d",
                        Toast.LENGTH_LONG).show();
                MainActivity.res_val=MainActivity.res_val+"3-d.";
                FragmentTransaction fg=getFragmentManager().beginTransaction();
                fg.replace(R.id.fagview,new QuestionFourFrag());
                fg.commit();
            }
        });

        return root;
    }

}
