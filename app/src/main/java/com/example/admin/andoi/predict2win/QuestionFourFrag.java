package com.example.admin.andoi.predict2win;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;


/**
 * A simple {@link Fragment} subclass.
 */
public class QuestionFourFrag extends Fragment {

    private TextView ques_four_display;
    private Button four_opta,four_optb,four_optc,four_optd;
    private DatabaseReference ques_val,opta,optb,optc,optd,fourques;
    public QuestionFourFrag() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View root=inflater.inflate(R.layout.fragment_question_four, container, false);

        ques_four_display=(TextView)root.findViewById(R.id.question_four_disp);
        four_opta=(Button)root.findViewById(R.id.ques_four_opta);
        four_optb=(Button)root.findViewById(R.id.ques_four_optb);
        four_optc=(Button)root.findViewById(R.id.ques_four_optc);
        four_optd=(Button)root.findViewById(R.id.ques_four_optd);
        fourques=FirebaseDatabase.getInstance().getReference().child("Question_four");
        fourques.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                ques_four_display.setText(dataSnapshot.child("Question_four_dis").getValue().toString());
                four_opta.setText(dataSnapshot.child("four_opta_dis").getValue().toString());
                four_optb.setText(dataSnapshot.child("four_optb_dis").getValue().toString());
                four_optc.setText(dataSnapshot.child("four_optc_dis").getValue().toString());
                four_optd.setText(dataSnapshot.child("four_optd_dis").getValue().toString());
                //String btn_status=dataSnapshot.child("FirstButton_enable").getValue().toString().toLowerCase();


            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        four_opta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                MainActivity.res_val=MainActivity.res_val+"4-a.";
                FragmentTransaction fg=getFragmentManager().beginTransaction();
                fg.replace(R.id.fagview,new QuestionFiveFrag());
                fg.commit();
            }
        });

        four_optb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                MainActivity.res_val=MainActivity.res_val+"4-b.";
                FragmentTransaction fg=getFragmentManager().beginTransaction();
                fg.replace(R.id.fagview,new QuestionFiveFrag());
                fg.commit();
            }
        });

        four_optc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                MainActivity.res_val=MainActivity.res_val+"4-c.";
                FragmentTransaction fg=getFragmentManager().beginTransaction();
                fg.replace(R.id.fagview,new QuestionFiveFrag());
                fg.commit();
            }
        });

        four_optd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                MainActivity.res_val=MainActivity.res_val+"4-d.";
                FragmentTransaction fg=getFragmentManager().beginTransaction();
                fg.replace(R.id.fagview,new QuestionFiveFrag());
                fg.commit();
            }
        });


        return root;    
    }

}
