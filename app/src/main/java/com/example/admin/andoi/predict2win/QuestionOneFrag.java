package com.example.admin.andoi.predict2win;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;


/**
 * A simple {@link Fragment} subclass.
 */
public class QuestionOneFrag extends Fragment {

    private  TextView ques_one_display;
    private Button one_opta,one_optb,one_optc,one_optd;
    private DatabaseReference ques_val,opta,optb,optc,optd,btndetails,oneques;
    public QuestionOneFrag() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View root= inflater.inflate(R.layout.fragment_question_one, container, false);
        ques_one_display=(TextView)root.findViewById(R.id.question_one_disp);
        one_opta=(Button)root.findViewById(R.id.ques_one_opta);
        one_optb=(Button)root.findViewById(R.id.ques_one_optb);
        one_optc=(Button)root.findViewById(R.id.ques_one_optc);
        one_optd=(Button)root.findViewById(R.id.ques_one_optd);
        oneques=FirebaseDatabase.getInstance().getReference().child("Question_one");
        oneques.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                ques_one_display.setText(dataSnapshot.child("Question_one_dis").getValue().toString());
                one_opta.setText(dataSnapshot.child("one_opta_dis").getValue().toString());
                one_optb.setText(dataSnapshot.child("one_optb_dis").getValue().toString());
                one_optc.setText(dataSnapshot.child("one_optc_dis").getValue().toString());
                one_optd.setText(dataSnapshot.child("one_optd_dis").getValue().toString());
                String btn_status=dataSnapshot.child("FirstButton_enable").getValue().toString().toLowerCase();
                if(btn_status.equals("disable")){
                    one_optc.setEnabled(false);
                    one_optd.setEnabled(false);

                }else{
                    one_optc.setEnabled(true);
                    one_optd.setEnabled(true);
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        one_opta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                MainActivity.res_val=MainActivity.res_val+"1-a.";
                FragmentTransaction fg=getFragmentManager().beginTransaction();
                fg.replace(R.id.fagview,new QuestionTwoFrag());
                fg.commit();
            }
        });

        one_optb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                MainActivity.res_val=MainActivity.res_val+"1-b.";
                FragmentTransaction fg=getFragmentManager().beginTransaction();
                fg.replace(R.id.fagview,new QuestionTwoFrag());
                fg.commit();
            }
        });

        one_optc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                MainActivity.res_val=MainActivity.res_val+"1-c.";
                FragmentTransaction fg=getFragmentManager().beginTransaction();
                fg.replace(R.id.fagview,new QuestionTwoFrag());
                fg.commit();
            }
        });

        one_optd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                MainActivity.res_val=MainActivity.res_val+"1-d.";
                FragmentTransaction fg=getFragmentManager().beginTransaction();
                fg.replace(R.id.fagview,new QuestionTwoFrag());
                fg.commit();
            }
        });


        return root;
    }


}
