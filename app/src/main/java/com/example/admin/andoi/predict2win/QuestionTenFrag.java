package com.example.admin.andoi.predict2win;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;


/**
 * A simple {@link Fragment} subclass.
 */
public class QuestionTenFrag extends Fragment {

    private TextView ques_ten_display;
    private Button ten_opta,ten_optb,ten_optc,ten_optd;
    private DatabaseReference ques_val,opta,optb,optc,optd,tenques;
    public QuestionTenFrag() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View root=inflater.inflate(R.layout.fragment_question_ten, container, false);

        ques_ten_display=(TextView)root.findViewById(R.id.question_ten_disp);
        ten_opta=(Button)root.findViewById(R.id.ques_ten_opta);
        ten_optb=(Button)root.findViewById(R.id.ques_ten_optb);
        ten_optc=(Button)root.findViewById(R.id.ques_ten_optc);
        ten_optd=(Button)root.findViewById(R.id.ques_ten_optd);
        tenques=FirebaseDatabase.getInstance().getReference().child("Question_ten");
        tenques.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                ques_ten_display.setText(dataSnapshot.child("Question_ten_dis").getValue().toString());
                ten_opta.setText(dataSnapshot.child("ten_opta_dis").getValue().toString());
                ten_optb.setText(dataSnapshot.child("ten_optb_dis").getValue().toString());
                ten_optc.setText(dataSnapshot.child("ten_optc_dis").getValue().toString());
                ten_optd.setText(dataSnapshot.child("ten_optd_dis").getValue().toString());
                //String btn_status=dataSnapshot.child("FirstButton_enable").getValue().toString().toLowerCase();


            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        ten_opta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                MainActivity.res_val=MainActivity.res_val+"10-a";
                FragmentTransaction fg=getFragmentManager().beginTransaction();
                fg.replace(R.id.fagview,new StoreResultFrag());
                fg.commit();
            }
        });

        ten_optb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                MainActivity.res_val=MainActivity.res_val+"10-b";
                FragmentTransaction fg=getFragmentManager().beginTransaction();
                fg.replace(R.id.fagview,new StoreResultFrag());
                fg.commit();
            }
        });

        ten_optc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                MainActivity.res_val=MainActivity.res_val+"10-c";
                FragmentTransaction fg=getFragmentManager().beginTransaction();
                fg.replace(R.id.fagview,new StoreResultFrag());
                fg.commit();
            }
        });

        ten_optd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                MainActivity.res_val=MainActivity.res_val+"10-d";

                FragmentTransaction fg=getFragmentManager().beginTransaction();
                fg.replace(R.id.fagview,new StoreResultFrag());
                fg.commit();
            }
        });
        
        return root;
    }

}
