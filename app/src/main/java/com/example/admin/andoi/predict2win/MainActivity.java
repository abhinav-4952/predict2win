package com.example.admin.andoi.predict2win;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

public class MainActivity extends AppCompatActivity  implements Register_DialogBox.ExampleListener{
    public EditText email;
    private EditText pass;
    private Button signin;
    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;
    public static String res_val="";
    private TextView registertxt;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        email=(EditText)findViewById(R.id.username);
        pass=(EditText)findViewById(R.id.password);
        signin=(Button)findViewById(R.id.login);
        registertxt=(TextView)findViewById(R.id.register);
        mAuth=FirebaseAuth.getInstance();
        mAuthListener=new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                if(firebaseAuth.getCurrentUser()!=null){
                    startActivity(new Intent(MainActivity.this,PassCodePage.class));
                }
            }
        };
        registertxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               openregisterdialog();
            }
        });
    }
    public void openregisterdialog(){
                Register_DialogBox rdb=new Register_DialogBox();
                rdb.show(getSupportFragmentManager(),"Regist");
    }


    public void goin(View v){
        goingIn();
    }

    @Override
    protected void onStart() {
        super.onStart();
        mAuth.addAuthStateListener(mAuthListener);
    }

    public void goingIn(){
        String e=email.getText().toString().trim();
        String p=pass.getText().toString().trim();
        if(TextUtils.isEmpty(e) || TextUtils.isEmpty(p)){
            Toast.makeText(MainActivity.this, "Enter details",
                    Toast.LENGTH_LONG).show();
        }
        mAuth.signInWithEmailAndPassword(e,p).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if(!task.isSuccessful()){
                    Toast.makeText(MainActivity.this, "Invalid",
                            Toast.LENGTH_LONG).show();
                }else{
                    Toast.makeText(MainActivity.this, "Success",
                            Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
    }

    @Override
    public void applyText(String useri, String passw) {
            email.setText(useri);
            pass.setText(passw);
            mAuth.createUserWithEmailAndPassword(useri,passw).addOnCompleteListener(MainActivity.this, new OnCompleteListener<AuthResult>() {
                @Override
                public void onComplete(@NonNull Task<AuthResult> task) {
                    if (!task.isSuccessful()) {
                        Toast.makeText(MainActivity.this, "Authentication failed." + task.getException(),
                                Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(MainActivity.this, "Registered",
                                Toast.LENGTH_LONG).show();

                    }
                }
            });
    }
}
