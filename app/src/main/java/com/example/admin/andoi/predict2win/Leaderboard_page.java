package com.example.admin.andoi.predict2win;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;

public class Leaderboard_page extends AppCompatActivity {
    private ListView board;
    private ArrayList<String> usernames=new ArrayList<>();
    private ArrayList<String> keys=new ArrayList<>();
    private DatabaseReference leaderboard_view;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_leaderboard_page);
        board=(ListView)findViewById(R.id.leaderboard);
        final ArrayAdapter<String> aryadapt=new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1,usernames);
        leaderboard_view= FirebaseDatabase.getInstance().getReference().child("Leader_Board").child("boardview");
        board.setAdapter(aryadapt);
        leaderboard_view.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                String value=dataSnapshot.getValue(String.class).replace("_b","\t\t\t\t\t\t\t\t\t\t\t");
                usernames.add(value);
                aryadapt.notifyDataSetChanged();
                String mkey=dataSnapshot.getKey();
                keys.add(mkey);

            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                String value=dataSnapshot.getValue(String.class).replace("_b","\t\t\t\t\t\t\t\t\t\t\t");
                String key=dataSnapshot.getKey();
                int index=keys.indexOf(key);
                usernames.set(index,value);
                aryadapt.notifyDataSetChanged();
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


    }


}
