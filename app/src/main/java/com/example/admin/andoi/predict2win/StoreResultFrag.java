package com.example.admin.andoi.predict2win;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 */
public class StoreResultFrag extends Fragment {

    private Button store_result;
    private EditText name;
    private DatabaseReference upload_res,player_list;
    private static int i;private ListView board;
    private ArrayList<String> playernames=new ArrayList<>();
    private ArrayList<String> playerkeys=new ArrayList<>();
    private DatabaseReference player_view;
    private ListView listt;

    public StoreResultFrag() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View root=inflater.inflate(R.layout.fragment_store_result, container, false);

        store_result=(Button)root.findViewById(R.id.store_res);
        name=(EditText)root.findViewById(R.id.req_name);
        upload_res= FirebaseDatabase.getInstance().getReference().child("Players").child("Results");
        player_list=FirebaseDatabase.getInstance().getReference().child("Players").child("Players_List");

        store_result.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String s1=name.getText().toString();
                if(s1.isEmpty()){
                    Toast.makeText(getActivity(), "Enter your Name",
                            Toast.LENGTH_LONG).show();
                }else{
                    storeoptions(s1,i++);
                    MainActivity.res_val="";

                    FragmentTransaction fg=getFragmentManager().beginTransaction();
                    fg.replace(R.id.fagview,new PassCodeFrag());
                    fg.commit();
                }
            }
        });
        listt=(ListView)root.findViewById(R.id.play_list);
        final ArrayAdapter<String> aryadapt=new ArrayAdapter<String>(getActivity(),android.R.layout.simple_list_item_1,playernames);
        player_view=FirebaseDatabase.getInstance().getReference().child("Players").child("Players_List");
        listt.setAdapter(aryadapt);
        player_view.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                String value=dataSnapshot.getValue(String.class);
                if(value!="Sample"){
                    playernames.add(value);
                    aryadapt.notifyDataSetChanged();
                    String mkey=dataSnapshot.getKey();
                    playerkeys.add(mkey);
                }
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                String value=dataSnapshot.getValue(String.class);
                String key=dataSnapshot.getKey();
                int index=playerkeys.indexOf(key);
                playernames.set(index,value);
                aryadapt.notifyDataSetChanged();
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });




        return root;
    }
    public void storeoptions(String user,int i){
            upload_res.child(user).setValue(MainActivity.res_val);
            player_list.child(user).setValue(user);
    }


}
