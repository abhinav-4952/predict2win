package com.example.admin.andoi.predict2win;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;


/**
 * A simple {@link Fragment} subclass.
 */
public class QuestionSixFrag extends Fragment {

    private TextView ques_six_display;
    private Button six_opta,six_optb,six_optc,six_optd;
    private DatabaseReference ques_val,opta,optb,optc,optd,sixques;
    public QuestionSixFrag() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View root=inflater.inflate(R.layout.fragment_question_six, container, false);

        ques_six_display=(TextView)root.findViewById(R.id.question_six_disp);
        six_opta=(Button)root.findViewById(R.id.ques_six_opta);
        six_optb=(Button)root.findViewById(R.id.ques_six_optb);
        six_optc=(Button)root.findViewById(R.id.ques_six_optc);
        six_optd=(Button)root.findViewById(R.id.ques_six_optd);
        sixques=FirebaseDatabase.getInstance().getReference().child("Question_six");
        sixques.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                ques_six_display.setText(dataSnapshot.child("Question_six_dis").getValue().toString());
                six_opta.setText(dataSnapshot.child("six_opta_dis").getValue().toString());
                six_optb.setText(dataSnapshot.child("six_optb_dis").getValue().toString());
                six_optc.setText(dataSnapshot.child("six_optc_dis").getValue().toString());
                six_optd.setText(dataSnapshot.child("six_optd_dis").getValue().toString());
                //String btn_status=dataSnapshot.child("FirstButton_enable").getValue().toString().toLowerCase();


            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        six_opta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                MainActivity.res_val=MainActivity.res_val+"6-a.";
                FragmentTransaction fg=getFragmentManager().beginTransaction();
                fg.replace(R.id.fagview,new QuestionSevenFrag());
                fg.commit();
            }
        });

        six_optb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                MainActivity.res_val=MainActivity.res_val+"6-b.";
                FragmentTransaction fg=getFragmentManager().beginTransaction();
                fg.replace(R.id.fagview,new QuestionSevenFrag());
                fg.commit();
            }
        });

        six_optc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                MainActivity.res_val=MainActivity.res_val+"6-c.";
                FragmentTransaction fg=getFragmentManager().beginTransaction();
                fg.replace(R.id.fagview,new QuestionSevenFrag());
                fg.commit();
            }
        });

        six_optd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                MainActivity.res_val=MainActivity.res_val+"6-d.";
                FragmentTransaction fg=getFragmentManager().beginTransaction();
                fg.replace(R.id.fagview,new QuestionSevenFrag());
                fg.commit();
            }
        });



        return root;
    }

}
