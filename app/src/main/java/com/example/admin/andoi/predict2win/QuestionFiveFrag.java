package com.example.admin.andoi.predict2win;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;


/**
 * A simple {@link Fragment} subclass.
 */
public class QuestionFiveFrag extends Fragment {

    private TextView ques_five_display;
    private Button five_opta,five_optb,five_optc,five_optd;
    private DatabaseReference ques_val,opta,optb,optc,optd,fiveques;
    public QuestionFiveFrag() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View root=inflater.inflate(R.layout.fragment_question_five, container, false);

        ques_five_display=(TextView)root.findViewById(R.id.question_five_disp);
        five_opta=(Button)root.findViewById(R.id.ques_five_opta);
        five_optb=(Button)root.findViewById(R.id.ques_five_optb);
        five_optc=(Button)root.findViewById(R.id.ques_five_optc);
        five_optd=(Button)root.findViewById(R.id.ques_five_optd);
        fiveques=FirebaseDatabase.getInstance().getReference().child("Question_five");
        fiveques.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                ques_five_display.setText(dataSnapshot.child("Question_five_dis").getValue().toString());
                five_opta.setText(dataSnapshot.child("five_opta_dis").getValue().toString());
                five_optb.setText(dataSnapshot.child("five_optb_dis").getValue().toString());
                five_optc.setText(dataSnapshot.child("five_optc_dis").getValue().toString());
                five_optd.setText(dataSnapshot.child("five_optd_dis").getValue().toString());
                //String btn_status=dataSnapshot.child("FirstButton_enable").getValue().toString().toLowerCase();


            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        five_opta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                MainActivity.res_val=MainActivity.res_val+"5-a.";
                FragmentTransaction fg=getFragmentManager().beginTransaction();
                fg.replace(R.id.fagview,new QuestionSixFrag());
                fg.commit();
            }
        });

        five_optb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                MainActivity.res_val=MainActivity.res_val+"5-b.";
                FragmentTransaction fg=getFragmentManager().beginTransaction();
                fg.replace(R.id.fagview,new QuestionSixFrag());
                fg.commit();
            }
        });

        five_optc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                MainActivity.res_val=MainActivity.res_val+"5-c.";
                FragmentTransaction fg=getFragmentManager().beginTransaction();
                fg.replace(R.id.fagview,new QuestionSixFrag());
                fg.commit();
            }
        });

        five_optd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                MainActivity.res_val=MainActivity.res_val+"5-d.";
                FragmentTransaction fg=getFragmentManager().beginTransaction();
                fg.replace(R.id.fagview,new QuestionSixFrag());
                fg.commit();
            }
        });


        return root;
    }

}
