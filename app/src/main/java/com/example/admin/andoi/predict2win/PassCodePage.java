package com.example.admin.andoi.predict2win;

import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class PassCodePage extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pass_code_page);
        FragmentTransaction fragtrans=getSupportFragmentManager().beginTransaction();
        fragtrans.add(R.id.fagview,new PassCodeFrag());
        fragtrans.commit();
    }

    @Override
    public void onBackPressed() {
       // super.onBackPressed();
    }
}
